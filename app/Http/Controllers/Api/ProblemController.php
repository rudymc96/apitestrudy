<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Problem1Service;
use App\Services\Problem2Service;
use Illuminate\Http\Request;

class ProblemController extends Controller
{

    /**
     * @var Problem1Service
     */
    protected $problem1Service;

    /**
     * @var Problem2Service
     */
    protected $problem2Service;

    /**
     * Problem1Controller constructor.
     * @param Problem1Service $problem1Service
     * @param Problem2Service $problem2Service
     */
    public function __construct(Problem1Service $problem1Service, Problem2Service $problem2Service)
    {
        $this->problem1Service = $problem1Service;
        $this->problem2Service = $problem2Service;
    }

    public function problem1(Request $request) {

        $result = $this->problem1Service->problem1($request->input());

        return response()->json($result);
    }

    public function problem2(Request $request) {

        $result = $this->problem2Service->problem2($request->input());

        return response()->json($result);
    }
}
