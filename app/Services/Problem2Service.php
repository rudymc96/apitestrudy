<?php

namespace App\Services;

class Problem2Service
{
    /**
     * Problem1Service constructor.
     */
    public function __construct()
    {
    }

    public function problem2 ($data = []) {
        $lineas = explode("\n", $data['input']);
        $output = "";

        foreach ($lineas as $linea) {
            $simbolos = [];
            $result = "";

            if ($linea <= 10000) {

                $caracteres = str_split($linea);

                $length = count($caracteres) - 1;

                for ($i = 0; $i <= $length; $i++) {

                    if ($result !== "NO") {
                        switch ($caracteres[$i]) {
                            case '[':
                                array_push($simbolos, '[');
                                break;
                            case '{':
                                array_push($simbolos, '{');
                                break;
                            case '(':
                                array_push($simbolos, '(');
                                break;
                            case ']':
                                if (count($simbolos) > 0) {
                                    if ($simbolos[array_key_last($simbolos)] === '[') {
                                        $result = "YES";
                                        array_pop($simbolos);
                                    } else {
                                        $result = "NO";
                                    }
                                } else {
                                    $result = "NO";
                                }
                                break;
                            case '}':
                                if (count($simbolos) > 0) {
                                    if ($simbolos[array_key_last($simbolos)] === '{') {
                                        $result = "YES";
                                        array_pop($simbolos);
                                    } else {
                                        $result = "NO";
                                    }
                                } else {
                                    $result = "NO";
                                }
                                break;
                            case ')':
                                if (count($simbolos) > 0) {
                                    if ($simbolos[array_key_last($simbolos)] === '(') {
                                        $result = "YES";
                                        array_pop($simbolos);
                                    } else {
                                        $result = "NO";
                                    }
                                } else {
                                    $result = "NO";
                                }
                                break;
                            default:
                                if ($result === "") {
                                    $result = "YES";
                                }
                                break;
                        }
                    }
                }
            }

            if (count($simbolos) > 0) {
                $result = "NO";
            }

            $output .= $result."\n";
        }

        $response = [
            "output" => $output
        ];
        return $response;
    }
}