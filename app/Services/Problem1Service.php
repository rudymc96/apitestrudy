<?php

namespace App\Services;

class Problem1Service
{
    /**
     * Problem1Service constructor.
     */
    public function __construct()
    {
    }

    public function problem1 ($data = []) {
        $tripletas = explode("\n", $data['input']);
        $totalTripletas = count($tripletas);
        $invalidInput = ["output" => "Is invalid"];

        if ($totalTripletas === 2) {
            $puntajeAlicia = explode(" ", $tripletas[0]);
            $puntajeBob = explode(" ", $tripletas[1]);

            $puntajes = [0, 0];

            if (count($puntajeAlicia) === 3 && count($puntajeBob) === 3) {
                for ($i = 0; $i <= 2; $i++) {
                    if (ctype_digit($puntajeAlicia[$i]) && ctype_digit($puntajeBob[$i])
                        && $puntajeAlicia[$i] <= 100 && $puntajeBob[$i] <= 100
                    ) {
                        if ($puntajeAlicia[$i] > $puntajeBob[$i]) {
                            $puntajes[0]++;
                        } elseif ($puntajeAlicia[$i] < $puntajeBob[$i]) {
                            $puntajes[1]++;
                        }
                    } else {
                        return $invalidInput;
                    }
                }
            } else {
                return $invalidInput;
            }
        } else {
            return $invalidInput;
        }

        $response = ["output" => "$puntajes[0] $puntajes[1]"];

        return $response;
    }

}