
# Pasos para instalar el proyecto

## Backend en Laravel 6

#### Configurar Laradock
* Paso 1
    * Tener laradock instalado 

* Paso 2
    * Duplicar un archivo de configuracion de una aplicacion en laravel, en la siguiente direccion
    
        > `/laradock/nginx/sites/`

* Paso 3
    * Cambiamos el nombre el archivo duplicado y lo dejamos con el nombre "apitestrudy.conf"

* Paso 4
    * Editamos el archivo poniendole las siguientes lineas
    
        > `server_name apitestrudy.test;`
        
        > `root /var/www/apitestrudy/public;`
        
* Paso 5
    * Editamos el archivo hosts del sistema mediante la siguiente linea de comandos
    
        > `sudo nano /etc/hosts`
        
    * Una vez dentro del archivo añadimos una nueva url de la siguiente manera
    
        > `127.0.0.1    apitestrudy.test`

* Paso 6
    * Detenemos los contenedores si estuvieran iniciados con el siguiente comando estando dentro de la carpeta laradock
    
        > `docker-compose down`

* Paso 7
    * Iniciamos los contenedores con el siguiente comando
    
        > `docker-compose up -d nginx mysql mailhog`
        
    * Siendo mailhog opcional

#### Configurar el proyecto

* Paso 1
    * Una vez levantados los contenedores accedemos a ellos con el siguiente comando, estando dentro de la carpeta laradock
    
        > `docker-compose exec --user=laradock workspace bash`

* Paso 2
    * Una vez dentro del contenedor clonamos el repositorio en la carpeta con sus proyectos con el siguiente comando:
    
        > `git clone https://bitbucket.org/rudymc96/apitestrudy.git`

* Paso 3
    * Accedemos a la carpeta del proyecto
    
        > `cd apitestrudy`
        
* Paso 4
    * Instalamos las dependencias en la carpeta del proyecto 
    
        > `composer install`
